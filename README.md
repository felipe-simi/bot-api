---

## Overview

This challenge was implemented in 5 microservices using well know patterns: API Gateway, Config Service, Naming Serive, Bots Service and Messages Service.

1. Bots service is responsible to handle all the information about the Bots.
2. Messages service takes care registring and consulting all messages exchanged between users and bots. No messages exchanged by user-user or bot-bot are allowed.

---

## Starting the services
1. Inside the Config Service folder, use the maven plugin (./mvnw spring-boot:run) or your preferred IDE.
2. Inside the Naming Service folder, use the maven plugin (./mvnw spring-boot:run) or your preferred IDE.
3. Inside the API Gateway folder, use the maven plugin (./mvnw spring-boot:run) or your preferred IDE.
4. Inside the Bots Service folder, use the maven plugin (./mvnw spring-boot:run) or your preferred IDE.
5. Inside the Messages Service folder, use the maven plugin (./mvnw spring-boot:run) or your preferred IDE.

---

## Future releases

- Create automated tests
- Run applications inside Docker containers
- Migrate H2 (in memory database) to MySQL or Postgres
- Generate APIs documentation

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
