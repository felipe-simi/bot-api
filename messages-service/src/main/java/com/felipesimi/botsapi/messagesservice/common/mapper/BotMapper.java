package com.felipesimi.botsapi.messagesservice.common.mapper;

import com.felipesimi.botsapi.messagesservice.common.dto.BotDto;
import com.felipesimi.botsapi.messagesservice.common.exception.BotNotFoundExcetion;
import com.felipesimi.botsapi.messagesservice.common.exception.InvalidCommunicationException;
import com.felipesimi.botsapi.messagesservice.common.proxy.BotServiceProxy;
import com.felipesimi.botsapi.messagesservice.message.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BotMapper {

    private final BotServiceProxy botProxy;

    public BotMapper(BotServiceProxy botProxy){
        this.botProxy  = botProxy;
    }

    public BotDto getBotInConversation(Message message) {
        List<BotDto> bots = getBots(message);
        if (bots.size() > 1) {
            throw new InvalidCommunicationException("A bot can not communicate with other.");
        }
        return bots.get(0);
    }

    private List<BotDto> getBots(Message message) {
        List<String> ids = extractIdsFrom(message);
        return getExistingBotsInMessage(ids);
    }

    private List<String> extractIdsFrom(Message message) {
        List<String> ids = new ArrayList<>();
        ids.add(message.getFrom());
        ids.add(message.getTo());
        return ids;
    }

    private List<BotDto> getExistingBotsInMessage(List<String> ids) {
        ResponseEntity<List<BotDto>> listResponseEntity = this.botProxy.checkIfExists(ids);
        List<BotDto> body = listResponseEntity.getBody();

        if (body == null || body.isEmpty()) {
            throw new BotNotFoundExcetion();
        }

        return body;
    }

}
