package com.felipesimi.botsapi.messagesservice.message;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/messages")
public class MessageResource {


    private final MessageService messageService;

    public MessageResource(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Message>> getAllByConversationId(@RequestParam("conversationId") String id) {
        List<Message> messages = messageService.getByConversationId(id);
        ResponseEntity<List<Message>> botResource = new ResponseEntity<>(messages, HttpStatus.OK);
        return botResource;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Message> getBot(@PathVariable String id) {
        Message message = messageService.getById(id);
        ResponseEntity<Message> botResource = new ResponseEntity<>(message, HttpStatus.OK);
        return botResource;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    protected ResponseEntity<Message> create(@Valid @RequestBody Message message) {
        Message createdMessage = messageService.create(message);
        return new ResponseEntity<>(createdMessage, HttpStatus.CREATED);

    }


}
