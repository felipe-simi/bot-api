package com.felipesimi.botsapi.messagesservice.message;

import com.felipesimi.botsapi.messagesservice.common.exception.InvalidCommunicationException;
import com.felipesimi.botsapi.messagesservice.common.exception.MessageNotFoundExcetion;
import com.felipesimi.botsapi.messagesservice.common.mapper.BotMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    private final MessageRepository messageRepository;

    private final BotMapper botMapper;

    public MessageService(MessageRepository messageRepository, BotMapper botMapper) {
        this.messageRepository = messageRepository;
        this.botMapper = botMapper;
    }

    protected List<Message> getByConversationId(String id) {
        return this.messageRepository.findAllByConversationId(id);
    }

    protected Message getById(String id) {
        Optional<Message> messageOptional = this.messageRepository.findById(id);
        return messageOptional.orElseThrow(() -> new MessageNotFoundExcetion(id));
    }

    protected Message create(Message message) {
        validateSenderReceiver(message);
        botMapper.getBotInConversation(message);
        Message createdMessage = messageRepository.save(message);
        return createdMessage;
    }

    private void validateSenderReceiver(Message message) {
        String sender = message.getFrom();
        String receiver = message.getTo();
        if (sender.equals(receiver)) {
            throw new InvalidCommunicationException("Sender and receiver cannot be equal");
        }
    }

}
