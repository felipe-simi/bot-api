package com.felipesimi.botsapi.messagesservice.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BotNotFoundExcetion extends RuntimeException {

    public BotNotFoundExcetion() {
        super("Bot could not be found");
    }

}