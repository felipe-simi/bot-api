package com.felipesimi.botsapi.messagesservice.common.proxy;

import com.felipesimi.botsapi.messagesservice.common.dto.BotDto;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "api-gateway")
@RibbonClient(name = "bots-service")
public interface BotServiceProxy {

    @GetMapping(value = "/bots-service/v1/bots")
    public ResponseEntity<List<BotDto>> checkIfExists(@RequestParam("ids") List<String> ids);

}
