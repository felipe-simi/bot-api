package com.felipesimi.botsapi.messagesservice.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MessageNotFoundExcetion extends RuntimeException {

    public MessageNotFoundExcetion(String id) {
        super("Message with id " + id + " could not be found");
    }

}