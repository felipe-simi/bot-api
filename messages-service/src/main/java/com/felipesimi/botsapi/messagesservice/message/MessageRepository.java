package com.felipesimi.botsapi.messagesservice.message;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, String> {

    List<Message> findAllByConversationId(String id);

}
