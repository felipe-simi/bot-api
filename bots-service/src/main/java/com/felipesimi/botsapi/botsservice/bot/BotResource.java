package com.felipesimi.botsapi.botsservice.bot;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("v1/bots")
public class BotResource {

    private final BotService botService;

    public BotResource(BotService botService) {
        this.botService = botService;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<Bot>> checkIfExists(@RequestParam("ids") List<String> ids) {
        List<Bot> bots = botService.getByIds(ids);
        ResponseEntity<List<Bot>> botResource = new ResponseEntity<>(bots, HttpStatus.OK);
        return botResource;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Bot> getBot(@PathVariable String id) {
        Bot bot = botService.getById(id);
        ResponseEntity<Bot> botResource = new ResponseEntity<>(bot, HttpStatus.OK);
        return botResource;
    }

    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<Bot> createBot(@Valid @RequestBody Bot bot) {
        Bot createdBot = botService.create(bot);
        return new ResponseEntity<Bot>(createdBot, HttpStatus.CREATED);

    }

    @PutMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<Bot> updateBot(
            @PathVariable String id,
            @RequestBody Bot bot) {
        Bot createdBot = botService.update(id, bot);
        return new ResponseEntity<Bot>(createdBot, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity delete(@PathVariable String id) {
        botService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
