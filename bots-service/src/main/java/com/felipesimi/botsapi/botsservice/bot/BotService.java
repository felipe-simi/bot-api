package com.felipesimi.botsapi.botsservice.bot;

import com.felipesimi.botsapi.botsservice.common.exception.BotAlreadyExistsExcetion;
import com.felipesimi.botsapi.botsservice.common.exception.BotNotFoundExcetion;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BotService {

    private final BotRepository botRepository;

    protected BotService(BotRepository botRepository) {
        this.botRepository = botRepository;
    }

    protected List<Bot> getByIds(List<String> ids) {
        return botRepository.findAllById(ids);
    }

    protected Bot getById(String id) {
        Optional<Bot> botOptional = this.botRepository.findById(id);
        return botOptional.orElseThrow(() -> new BotNotFoundExcetion(id));
    }

    protected Bot create(Bot bot) {
        validateConflict(bot);
        return botRepository.save(bot);
    }

    private void validateConflict(Bot bot) {
        Optional<Bot> botOptional = this.botRepository.findById(bot.getId());
        if (botOptional.isPresent()) {
            throw new BotAlreadyExistsExcetion(bot.getId());
        }
    }

    protected Bot update(String id, Bot updatedBot) {
        Bot existedBot = this.getById(id);
        existedBot.merge(updatedBot);
        this.botRepository.save(existedBot);
        return existedBot;
    }

    protected void delete(String id) {
        Bot existedBot = this.getById(id);
        this.botRepository.delete(existedBot);
    }
}
