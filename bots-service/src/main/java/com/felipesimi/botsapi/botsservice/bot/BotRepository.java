package com.felipesimi.botsapi.botsservice.bot;

import org.springframework.data.jpa.repository.JpaRepository;

public interface BotRepository extends JpaRepository<Bot, String> {
}
