package com.felipesimi.botsapi.botsservice.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BotNotFoundExcetion extends RuntimeException {

    public BotNotFoundExcetion(String id) {
        super("Bot with id " + id + " could not be found");
    }

}