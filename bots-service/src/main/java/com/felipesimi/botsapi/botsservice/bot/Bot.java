package com.felipesimi.botsapi.botsservice.bot;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class Bot {

    @Id
    @NotNull(message = "Id can not be null")
    @Pattern(regexp = "^[^/]+$", message = "Id can not contain '/'")
    private String id;

    @NotNull(message = "Name can not be null")
    private String name;

    public Bot() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Bot{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public void merge(Bot updatedBot) {

        if (updatedBot == null) {
            return;
        }

        if (updatedBot.name != null) {
            this.name = updatedBot.name;
        }

    }
}
