package com.felipesimi.botsapi.botsservice.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class BotAlreadyExistsExcetion extends RuntimeException {

    public BotAlreadyExistsExcetion(String id) {
        super("Bot with id " + id + " already exists");
    }

}